import { IOverrideRequest } from '../services/utilities/apiResponse.service';

export interface IResponseError {
  error: {
    code: number;
    message: string;
  };
  success: boolean;
  override?: IOverrideRequest;
}
