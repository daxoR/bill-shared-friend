import { User } from '../../entities/user/userEntity.entity';

export interface IUserNoPassword extends Omit<User, 'password'> {}
