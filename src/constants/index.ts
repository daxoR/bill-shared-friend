import cookie from './cookie';
import errors from './errors';

export default {
  Cookie: cookie,
  Errors: errors,
};
