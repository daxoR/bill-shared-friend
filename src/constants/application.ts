export default {
  url: {
    base: '/api',
  },
  env: {
    authSecret: process.env.TOKEN_SECRET_KEY || 'test',
  },
  timers: {
    userCookieExpiry: '720h',
  },
};
