import httpStatusCodes from 'http-status-codes';
import IController from 'IController';
import apiResponse, { ICookie } from '../services/utilities/apiResponse.service';
import constants from '../constants';
import userService from '../services/user.service';

import { generateToken } from '../services/utilities/encryption.service';
import { IUserNoPassword } from 'user/IUserNoPassword';

const register: IController = async (req, res): Promise<void> => {
  let user: IUserNoPassword;
  try {
    const { email, password, name } = req.body;
    user = await userService.registerUser(email, password, name);
  } catch (err) {
    console.log(err);
    if (err.code === constants.Errors.DUPLICATE_ENTRY) {
      apiResponse.error(res, httpStatusCodes.BAD_REQUEST, 'Email already exist');
      return;
    } else {
      apiResponse.error(res, httpStatusCodes.BAD_REQUEST);
    }
    return;
  }
  if (user) {
    const cookie = await generateUserToken(user.id);
    apiResponse.result(res, user, httpStatusCodes.OK, cookie);
  } else {
    apiResponse.error(res, httpStatusCodes.BAD_REQUEST);
  }
};

const login: IController = async (req, res): Promise<void> => {
  const { email, password } = req.body;
  const user = await userService.loginUser(email, password);
  if (user) {
    const cookie = await generateUserToken(user.id);
    apiResponse.result(res, user, httpStatusCodes.OK, cookie);
  } else {
    apiResponse.error(res, httpStatusCodes.BAD_REQUEST, 'Invalid credentials');
  }
};

const generateUserToken = async (userId: number): Promise<ICookie> => {
  return {
    key: constants.Cookie.COOKIE_USER,
    value: await generateToken(constants.Cookie.KEY_USER_ID, userId.toString()),
  };
};

export default {
  login,
  register,
};
