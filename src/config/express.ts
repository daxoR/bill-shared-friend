import bodyParser from 'body-parser';
import express from 'express';
import joiErrorHandler from '../middlewares/joiErrorHandler';
import application from '../constants/application';
import indexRoute from '../routes/index.route';
import apiErrorHandler from '../middlewares/apiErrorHandler';

const app = express();

import dotenv = require('dotenv');
dotenv.config();
app.use(bodyParser.json());

// Router
app.use(application.url.base, indexRoute);

app.use(joiErrorHandler);

app.use(apiErrorHandler.notFoundErrorHandler);

export default app;
