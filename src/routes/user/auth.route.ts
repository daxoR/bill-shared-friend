import express from 'express';

const router = express.Router();
import { celebrate } from 'celebrate';
import userSchema from '../../constants/schema/user.schema';
import userController from '../../controllers/user.controller';

router.post('/login', celebrate(userSchema.login), userController.login);
router.post('/register', celebrate(userSchema.register), userController.register);

export default router;
