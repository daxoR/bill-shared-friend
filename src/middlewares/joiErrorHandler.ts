import { CelebrateError, isCelebrateError } from 'celebrate';
import express from 'express';
import { ValidationError } from 'joi';
import HttpStatusCodes from 'http-status-codes';
import { IResponseError } from 'IResponseError';

export default (
  err: CelebrateError,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
): unknown => {
  if (isCelebrateError(err)) {
    const details: string[] = [];
    err.details.forEach((error1: ValidationError) => error1.details.forEach((value) => details.push(value.message)));
    const error: IResponseError = {
      override: {
        message: details.join(','),
        code: 1001,
      },
      error: {
        code: HttpStatusCodes.BAD_REQUEST,
        message: 'At least one required field missing',
      },
      success: false,
    };
    return res.status(HttpStatusCodes.BAD_REQUEST).json(error);
  }
  return next(err);
};
