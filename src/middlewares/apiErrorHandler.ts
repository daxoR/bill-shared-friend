import express from 'express';
import HttpStatusCodes from 'http-status-codes';

export function notFoundErrorHandler(req: express.Request, res: express.Response): void {
  res.status(HttpStatusCodes.NOT_FOUND).json({
    success: false,
    error: {
      code: HttpStatusCodes.NOT_FOUND,
      message: HttpStatusCodes.getStatusText(HttpStatusCodes.NOT_FOUND),
    },
  });
}

export default {
  notFoundErrorHandler,
};
