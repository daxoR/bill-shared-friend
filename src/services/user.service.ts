import { getRepository } from 'typeorm';
import { IUserNoPassword } from 'user/IUserNoPassword';
import { User } from '../entities/user/userEntity.entity';
import { generateHash, verifyHash } from './utilities/encryption.service';

const registerUser = async (email: string, password: string, name: string): Promise<IUserNoPassword> => {
  const newUser = new User();
  newUser.email = email;
  newUser.password = await generateHash(password, 10);
  newUser.name = name;
  return sanitizeUser(await getRepository(User).save(newUser));
};

const loginUser = async (email: string, password: string): Promise<IUserNoPassword> => {
  const user = await getUserByEmail(email);
  if (user) {
    if (await verifyHash(password, user.password)) {
      user.lastLogin = new Date();
      updateUser(user);
      return sanitizeUser(user);
    }
  }
  return null;
};

const getUserById = async (userId: number): Promise<IUserNoPassword> => {
  try {
    return await sanitizeUser(await getRepository(User).findOne({ id: userId }));
  } catch (e) {
    return null;
  }
};

const getUserByEmail = async (email: string): Promise<User> => {
  try {
    return await getRepository(User).findOne({ email });
  } catch (e) {
    return null;
  }
};

const updateUser = async (user: User): Promise<User> => {
  return await getRepository(User).save(user);
};

const sanitizeUser = (user: User): IUserNoPassword => {
  const _user = { ...user };
  delete _user.password;
  return _user;
};

export default {
  loginUser,
  registerUser,
};
