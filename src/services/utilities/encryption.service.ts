import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import application from '../../constants/application';

export const verifyHash = async (password: string, hash: string): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    const result = bcrypt.compare(password, hash);
    if (result) {
      resolve(true);
    }
    reject();
  });
};

export const generateHash = async (password: string, salt: number): Promise<string> => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, salt, (err: Error, hash: string) => {
      if (!err) {
        resolve(hash);
      }
      reject(err);
    });
  });
};

export const generateToken = async (key: string, value: string): Promise<string> => {
  const data: { [key: string]: string } = {};
  data[key] = value;
  return await jwt.sign({ data }, application.env.authSecret, {
    expiresIn: application.timers.userCookieExpiry,
  });
};
