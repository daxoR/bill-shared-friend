import { Response } from 'express';
import httpStatusCodes from 'http-status-codes';
import { IResponseError } from 'IResponseError';

export interface IOverrideRequest {
  code: number;
  message: string;
}

export interface ICookie {
  key: string;
  value: string;
}

export default class ApiResponse {
  static result = (res: Response, data: Record<string, unknown>, status = 200, cookie: ICookie = null): void => {
    res.status(status);
    if (cookie) {
      res.cookie(cookie.key, cookie.value);
    }
    res.json({
      data,
      success: true,
    });
  };

  static error = (
    res: Response,
    status = 400,
    error: string = httpStatusCodes.getStatusText(status),
    override: IOverrideRequest = null,
  ): void => {
    const response: IResponseError = {
      override,
      error: {
        code: status,
        message: error,
      },
      success: false,
    };
    res.status(status).json(response);
  };
}
